package com.Vishalaxi.android.Xploree;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.location.Location;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.Toast;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;

/*Provides UI for the main screen.*/
public class MainActivity extends AppCompatActivity {
    final int LOCATION_REQUEST_CODE = 1;
    final static String EXTRA_LATITUDE = "lat_extra";
    final static String EXTRA_LONGITUDE = "long_extra";
    final static String EXTRA_PARCELABLE_PLACE_ARRAY = "parcelable_place_array";
    private Toolbar mActionBarToolbar;
    private LocationManager myLocationMgr;
    private Location myLocation; //User's current location
    private int clickPosition = 0;
    private GridView gridView;
    private String[] mPlaceType = null; // A String array containing place types displayed to user
    private PlacesOfInterestAdapter placesOfInterestAdapter;
    public static Bitmap[] bitmapArray;
    private PlacesOfInterest[] places = {
            new PlacesOfInterest(1, R.drawable.hospital_1),
            new PlacesOfInterest(2, R.drawable.school_1),
            new PlacesOfInterest(3, R.drawable.food_1),
            new PlacesOfInterest(4, R.drawable.bus_1),
            new PlacesOfInterest(5, R.drawable.bank_1),
            new PlacesOfInterest(6, R.drawable.airport_1),
            new PlacesOfInterest(7, R.drawable.church_1),
            new PlacesOfInterest(8, R.drawable.movie_1),
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
        setProgressBarIndeterminateVisibility(true);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity);
        mActionBarToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mActionBarToolbar);
        getSupportActionBar().setTitle("Xploree");
        FragmentsActivity.fragment_type = -1;

        gridView = (GridView) findViewById(R.id.gridview);
        placesOfInterestAdapter = new PlacesOfInterestAdapter(this, places);
        myLocationMgr = (LocationManager) getSystemService(LOCATION_SERVICE);
        gridView.setAdapter(placesOfInterestAdapter);
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                FragmentsActivity.fragment_type = -1;
                clickPosition = position;
                if (haveNetworkConnection() == false) { //check if internet connection exists
                    Toast.makeText(MainActivity.this, "You need Internet to use this app!", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (isLocationEnabled(MainActivity.this)) { //check if location services are ON
                    /*For Android versions 6.0 (Marshmallow) and higher request  permissions at run time*/
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if (isLocationPermissionGiven()) { //check if permissions are given
                            myLocation = getCurrentLocation();
                            if (myLocation != null) {
                                startSearching(clickPosition);
                            }
                            return;
                        }
                        requestLocationPermission();
                    } else { /*For Android versions below marshmallow*/
                        myLocation = getCurrentLocation();
                        if (myLocation != null) {
                            startSearching(clickPosition);
                        }
                    }
                } else { // GPS or NETWORK provider is not available
                    showAlertDialogBox(); // notify user
                }
            }
        });
    }

    public void startSearching(int clickPosition) {//int position, PlacesOfInterestAdapter placesOfInterestAdapter){
        placesOfInterestAdapter.notifyDataSetChanged();
        mPlaceType = getResources().getStringArray(R.array.place_type);
        String types = mPlaceType[clickPosition];
        StringBuilder sb = new StringBuilder("https://maps.googleapis.com/maps/api/place/nearbysearch/json?");
        sb.append("location=" + myLocation.getLatitude() + "," + myLocation.getLongitude());
        sb.append("&rankby=distance");
        sb.append("&types=" + types);
        sb.append("&sensor=true");
        sb.append("&key=" + getString(R.string.key1));
        Log.i("testing", "string builder=" + sb);
        PlacesTask placesTask = new PlacesTask(); // Create new non-ui thread task to download Google place json data
        placesTask.execute(sb.toString());// Invokes the "doInBackground()" method of the class PlaceTask
    }

    /*A class, to download google places list*/
    private class PlacesTask extends AsyncTask<String, Integer, Place[]> {
        String data = null;

        // Invoked by execute() method of this object
        @Override
        protected void onPreExecute() {
            Log.i("Async Task Testing", "PlacesTask: onPreExecute()");
            // Show the progress bar while loading places
            LinearLayout headerProgress = (LinearLayout) findViewById(R.id.linlaHeaderProgress);
            headerProgress.setVisibility(View.VISIBLE);
        }

        /*Invoed on Background thread*/
        @Override
        protected Place[] doInBackground(String... url) {
            Log.i("Async Task Testing", "PlacesTask: doInBackground()");
            try {
                data = Utility.downloadUrl(url[0]);
            } catch (Exception e) {
                Log.d("Background Task", e.toString());
            }
            Place[] places = getJSONParsedPlaces(data);
            String photosURL[] = getPhotoURLArray(places);
            saveImageBitmaps(photosURL);
            return places;

        }

        // Executed after the complete execution of doInBackground() method
        @Override
        protected void onPostExecute(Place[] places) {
            Log.i("Async Task Testing", "PlacesTask: onPostExecute()");
            setProgressBarIndeterminateVisibility(false);
            LinearLayout progressBar = (LinearLayout) findViewById(R.id.linlaHeaderProgress);
            progressBar.setVisibility(View.GONE);
            Intent activityChangeIntent = new Intent(MainActivity.this, FragmentsActivity.class);
            activityChangeIntent.putParcelableArrayListExtra(EXTRA_PARCELABLE_PLACE_ARRAY,
                    new ArrayList(Arrays.asList(places)));
            if (myLocation != null) {
                activityChangeIntent.putExtra(EXTRA_LATITUDE, myLocation.getLatitude());
                activityChangeIntent.putExtra(EXTRA_LONGITUDE, myLocation.getLongitude());
            }
            startActivity(activityChangeIntent);
        }
    }

    void saveImageBitmaps(String[] photosURL) {
        bitmapArray = new Bitmap[photosURL.length];
        try {
            for (int i = 0; i < photosURL.length; i++) {
                // Starting image download
                bitmapArray[i] = Utility.downloadImage(photosURL[i]);
            }
        } catch (Exception e) {
            Log.d("Background Task", e.toString());
        }
    }

    Place[] getJSONParsedPlaces(String data) {
        Place[] places = null;
        PlaceJSONParser placeJsonParser = new PlaceJSONParser();
        JSONObject jObject;
        try {
            jObject = new JSONObject(data);
            places = placeJsonParser.parse(jObject); // Get the parsed data as a list
        } catch (Exception e) {
            Log.d("Exception", e.toString());
        }
        return places;
    }

    String[] getPhotoURLArray(Place[] places) {
        String[] urlArray = new String[places.length];
        for (int i = 0; i < places.length; i++) {
            if (places[i] != null) {
                Photo[] photos = places[i].getPhotos();
                int width = 500, height = 500;
                String url = "https://maps.googleapis.com/maps/api/place/photo?";
                String key = "key=" + getString(R.string.key1);
                String sensor = "sensor=true";
                String maxWidth = "maxwidth=" + width;
                String maxHeight = "maxheight=" + height;
                url = url + "&" + key + "&" + sensor + "&" + maxWidth + "&" + maxHeight;
                String photoReference = null;
                for (int j = 0; j < photos.length; j++) {
                    if (photos[j] != null) {
                        photoReference = "photoreference=" + photos[j].getPhotoReference();
                        url = url + "&" + photoReference; // URL for downloading the photo from Google Services
                        break;
                    }
                }
                if (photoReference == null) {
                    url = null;
                }
                urlArray[i] = url;
            }
        }
        return urlArray;
    }

    private void showAlertDialogBox() {
        AlertDialog.Builder dialog = new AlertDialog.Builder(MainActivity.this);
        dialog.setMessage("Switch ON Location");
        dialog.setPositiveButton("Open location settings", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                // TODO Auto-generated method stub
                Intent myIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivity(myIntent);
            }
        });
        dialog.setNegativeButton("Cancel", null);
        dialog.show();
    }

    private void requestLocationPermission() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_FINE_LOCATION)) {
            //If the user has denied the permission previously
            Toast.makeText(MainActivity.this, "You need the permission to use the app", Toast.LENGTH_SHORT).show();
        }
        //And finally ask for the permission
        ActivityCompat.requestPermissions(this, new String[]{
                Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.ACCESS_COARSE_LOCATION}, 1);
    }

    private Boolean isLocationPermissionGiven() {
        int result_fine = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION);
        int result_coarse = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION);
        if (result_fine == PackageManager.PERMISSION_GRANTED && result_coarse == PackageManager.PERMISSION_GRANTED) {
            return true;
        }
        return false;
    }

    public static boolean isLocationEnabled(Context context) {
        int locationMode = 0;
        String locationProviders;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            try {
                locationMode = Settings.Secure.getInt(context.getContentResolver(), Settings.Secure.LOCATION_MODE);
            } catch (Settings.SettingNotFoundException e) {
                e.printStackTrace();
                return false;
            }
            return locationMode != Settings.Secure.LOCATION_MODE_OFF;
        } else {
            locationProviders = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
            return !TextUtils.isEmpty(locationProviders);
        }
    }

    private Location getCurrentLocation() {
        try {
            myLocation = myLocationMgr.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
            if (myLocation == null) {
                myLocation = myLocationMgr.getLastKnownLocation(LocationManager.GPS_PROVIDER);
            }
        } catch (SecurityException se) {
            se.printStackTrace();
        }
        return myLocation;
    }

    private boolean haveNetworkConnection() {
        boolean haveConnectedWifi = false;
        boolean haveConnectedMobile = false;
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo[] netInfo = cm.getAllNetworkInfo();
        for (NetworkInfo ni : netInfo) {
            if (ni.getTypeName().equalsIgnoreCase("WIFI"))
                if (ni.isConnected())
                    haveConnectedWifi = true;
            if (ni.getTypeName().equalsIgnoreCase("MOBILE"))
                if (ni.isConnected())
                    haveConnectedMobile = true;
        }
        return haveConnectedWifi || haveConnectedMobile;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        Log.i("testing", "onRequestPermissionsResult()");
        switch (requestCode) {
            case LOCATION_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    myLocation = getCurrentLocation();
                    if (myLocation != null) {
                        startSearching(clickPosition);
                    }
                }
                break;
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
    }
}
