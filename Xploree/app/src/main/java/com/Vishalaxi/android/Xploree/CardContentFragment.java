package com.Vishalaxi.android.Xploree;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/* Provides UI for the view with Cards.*/
public class CardContentFragment extends Fragment {
    public static int position;
    static Place currentPlace;
    static Context context;
    static String keyString;
    private static Place[] localPlacesList;

    CardContentFragment( Place[] localPlacesList){
        this.localPlacesList = localPlacesList;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        RecyclerView recyclerView = (RecyclerView) inflater.inflate(
                R.layout.recycler_view, container, false);
        ContentAdapter adapter = new ContentAdapter(recyclerView.getContext());
        recyclerView.setAdapter(adapter);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        keyString=getString(R.string.key1);
        return recyclerView;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public ImageView picture;
        public TextView name;
        public TextView description;
        public ViewHolder(LayoutInflater inflater, ViewGroup parent) {
            super(inflater.inflate(R.layout.item_card, parent, false));
            picture = (ImageView) itemView.findViewById(R.id.card_image);
            name = (TextView) itemView.findViewById(R.id.card_title);
            description = (TextView) itemView.findViewById(R.id.card_text);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    position = getAdapterPosition();
                    currentPlace= localPlacesList[position % localPlacesList.length];
                    context = v.getContext();
                    StringBuilder sb = new StringBuilder("https://maps.googleapis.com/maps/api/place/details/json?");
                    sb.append("placeid="+currentPlace.getPlaceID());
                    sb.append("&key="+keyString);
                    // Creating a new non-ui thread task to download Google place json data
                    PlacesDetailsTask placesTask = new PlacesDetailsTask();
                    // Invokes the "doInBackground()" method of the class PlaceTask
                    Log.i("testing","place detail api="+sb.toString());
                    placesTask.execute(sb.toString());
                }
            });
        }
    }

    /* Adapter to display recycler view.*/
    public static class ContentAdapter extends RecyclerView.Adapter<ViewHolder> {
        // Set numbers of Card in RecyclerView.
        private  String[] mPlaces=new String[localPlacesList.length];
        private  String[] mPlaceDesc=new String[localPlacesList.length];
        public final static Drawable[]mPlacePictures=new Drawable[localPlacesList.length];

        public ContentAdapter(Context context) {
            Resources resources = context.getResources();
            for (int i = 0; i < localPlacesList.length; i++) {
                mPlaces[i] = localPlacesList[i].getPlaceName();
                mPlaceDesc[i] = localPlacesList[i].getVicinity();
                try {
                    if(MainActivity.bitmapArray[i]==null){
                        mPlacePictures[i] =null;
                    }else {
                        mPlacePictures[i] = new BitmapDrawable(resources, MainActivity.bitmapArray[i]);
                    }
                }catch(ArrayIndexOutOfBoundsException e){
                    Log.d("Exception",e.toString());
                }
            }
        }
        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            return new ViewHolder(LayoutInflater.from(parent.getContext()), parent);
        }
        @Override
        public void onBindViewHolder(ViewHolder holder, int position) {
            if(mPlacePictures[position % mPlacePictures.length]==null) {
                holder.picture.setImageResource(R.drawable.no_photo);
            }else{
                holder.picture.setImageDrawable(mPlacePictures[position % mPlacePictures.length]);
            }
            holder.name.setText(mPlaces[position % mPlaces.length]);
            holder.description.setText(mPlaceDesc[position % mPlaceDesc.length]);
        }

        @Override
        public int getItemCount() {
            return localPlacesList.length;

        }
    }

    /*A class, to download Google Places */
    private static class PlacesDetailsTask extends AsyncTask<String, Integer, Place> {
        // Invoked by execute() method of this object
        @Override
        protected Place doInBackground(String... url) {
            String jsonData=null;
            JSONObject jObject=null;
            try {
                jsonData = Utility.downloadUrl(url[0]);
                if(jsonData!=null){
                    jObject = new JSONObject(jsonData);
                }
                parsePlaceDetails(jObject);
            }catch (Exception e){
                Log.i("testing", "exception occured="+e.toString());
            }
            return currentPlace;
        }

        // Executed after the complete execution of doInBackground() method
        @Override
        protected void onPostExecute(Place placeResult){
            Intent intent = new Intent(context, PlaceDetailActivity.class);
            intent.putExtra(PlaceDetailActivity.EXTRA_POSITION, position);
            intent.putExtra(TileContentFragment.EXTRA_FRAGMENT,3);
            intent.putExtra(TileContentFragment.EXTRA_PARCELABLE_PLACE, (Parcelable) placeResult);
            context.startActivity(intent);
        }
    }
    static void parsePlaceDetails( JSONObject jObject){
        try {
            currentPlace.setPhoneNo(jObject.getJSONObject("result").getString("international_phone_number"));
            JSONArray reviews = jObject.getJSONObject("result").getJSONArray("reviews");
            String authorName, reviewText;
            if (reviews != null) {
                for (int i = 0; i < reviews.length(); i++) {
                    authorName = ((JSONObject) reviews.get(i)).getString("author_name");
                    reviewText = ((JSONObject) reviews.get(i)).getString("text");
                    reviewText = reviewText.replaceAll("\\s+", " ");
                    //currentPlace.mReviews += authorName + ":" + reviewText + "\n" + "\n";
                    currentPlace.setReviews(currentPlace.getReviews()+authorName + ":" + reviewText + "\n" + "\n");
                }
            }
            currentPlace.setWebsite(jObject.getJSONObject("result").getString("website"));
        }catch(JSONException je){
            je.printStackTrace();
        }
    }
}
