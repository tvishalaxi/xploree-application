package com.Vishalaxi.android.Xploree;

import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import java.util.ArrayList;
import java.util.List;

/*Manage all the fragments*/
public class FragmentsActivity extends AppCompatActivity {

    private Place[] fragmentPlacesList;
    private Double latitude,longitude;
    static int  fragment_type=0;
    private Toolbar mActionBarToolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragments_activity_main);
        mActionBarToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mActionBarToolbar);
        getSupportActionBar().setTitle("Xploree");
        Intent intent = getIntent();
        ArrayList<Place> list =intent.getParcelableArrayListExtra(MainActivity.EXTRA_PARCELABLE_PLACE_ARRAY);
        latitude = intent.getDoubleExtra(MainActivity.EXTRA_LATITUDE,0.0);
        longitude = intent.getDoubleExtra(MainActivity.EXTRA_LONGITUDE,0.0);
        if(list != null) {
            fragmentPlacesList = list.toArray(new Place[list.size()]);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        // Adding Toolbar to Main screen
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        // Setting ViewPager for each Tabs
        ViewPager viewPager = (ViewPager) findViewById(R.id.viewpager);
        setupViewPager(viewPager);
        // Set Tabs inside Toolbar
        TabLayout tabs = (TabLayout) findViewById(R.id.tabs);
        viewPager.setCurrentItem(fragment_type);
        //viewPager.setCurrentItem(0);
        tabs.setupWithViewPager(viewPager);

        if(fragment_type==-1) {
            fragment_type = 0;
        }
    }

    // Add Fragments to Tabs
    private void setupViewPager(ViewPager viewPager) {
        Adapter adapter = new Adapter(getSupportFragmentManager());
        adapter.addFragment(new MapFragment(fragmentPlacesList,latitude, longitude), "Maps");
        adapter.addFragment(new TileContentFragment(fragmentPlacesList), "Tile");
        adapter.addFragment(new ListContentFragment(fragmentPlacesList), "List");
        adapter.addFragment(new CardContentFragment(fragmentPlacesList), "Card");
        viewPager.setAdapter(adapter);
    }
    static class Adapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public Adapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        int id = item.getItemId();
        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            startActivity(new Intent(Settings.ACTION_SETTINGS));
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
