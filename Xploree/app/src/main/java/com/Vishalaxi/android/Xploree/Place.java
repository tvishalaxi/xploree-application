package com.Vishalaxi.android.Xploree;

/**
 * Created by VISH on 8/16/2016.
 */

import android.os.Parcel;
import android.os.Parcelable;

public class Place implements Parcelable {

    private String mLat = "";// Latitude of the place
    private String mLng = "";  // Longitude of the place
    private String mPlaceName = ""; // Place Name
    private String mVicinity = "";// Vicinity of the place
    private Photo[] mPhotos = {}; // Photos of the place Photo is a Parcelable class
    private String mReviews;  //user reviews
    private String mRating;   // user rating
    private String mPhoneNo;  //phone no
    private String mOpenNow;  //open now
    private String mWebsite;  //website url
    private String mPlaceID;  // placeID

    public String getLatitude() {
        return mLat;
    }
    public String getLongitude() {
        return mLng;
    }
    public String getPlaceName() {
        return mPlaceName;
    }
    public String getVicinity() {
        return mVicinity;
    }
    public Photo[] getPhotos() {
        return mPhotos;
    }
    public String getReviews() {
        return mReviews;
    }
    public String getRating() {
        return mRating;
    }
    public String getPhoneNo() {
        return mPhoneNo;
    }
    public String getWebsite() {
        return mWebsite;
    }
    public String getPlaceID() {
        return mPlaceID;
    }
    public String getOpenNow() {
        return mOpenNow;
    }
    public void setLatitude(String lat) {
        mLat = lat;
    }
    public void setLongitude(String lng) {
        mLng = lng;
    }
    public void setPlaceName(String placeName) {
        mPlaceName = placeName;
    }
    public void setVicinity(String vicinity) {
        mVicinity = vicinity;
    }
    public void setPhotos(Photo[] photos) {
        mPhotos = photos;
    }
    public void setReviews(String reviews) {
        mReviews = reviews;
    }
    public void setRating(String rating) {
        mRating = rating;
    }
    public void setPhoneNo(String phone) {
        mPhoneNo = phone;
    }
    public void setWebsite(String website) {
        mWebsite = website;
    }
    public void setPlaceID(String placeID) {
        mPlaceID = placeID;
    }
    public void setOpenNow(String openNow) {
        mOpenNow = openNow;
    }
    @Override
    public int describeContents() {
        // TODO Auto-generated method stub
        return 0;
    }

    /**
     * Writing Place object data to Parcel
     */
    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mLat);
        dest.writeString(mLng);
        dest.writeString(mPlaceName);
        dest.writeString(mVicinity);
        dest.writeString(mReviews);
        dest.writeString(mRating);
        dest.writeString(mPhoneNo);
        dest.writeString(mOpenNow);
        dest.writeString(mWebsite);
        dest.writeString(mPlaceID);
        // dest.writeParcelableArray(mPhotos, 0);
    }

    public Place() {
    }

    /*Initializing Place object from Parcel object*/
    private Place(Parcel in) {
        this.mLat = in.readString();
        this.mLng = in.readString();
        this.mPlaceName = in.readString();
        this.mVicinity = in.readString();
        this.mReviews = in.readString();
        this.mRating = in.readString();
        this.mPhoneNo = in.readString();
        this.mOpenNow = in.readString();
        this.mWebsite = in.readString();
        this.mPlaceID = in.readString();
        // this.mPhotos = (Photo[])in.readParcelableArray(Photo.class.getClassLoader());
    }

    /*Generates an instance of Place class from Parcel*/
    public static final Parcelable.Creator<Place> CREATOR = new Parcelable.Creator<Place>() {
        @Override
        public Place createFromParcel(Parcel source) {
            return new Place(source);
        }

        @Override
        public Place[] newArray(int size) {
            // TODO Auto-generated method stub
            return null;
        }
    };
}