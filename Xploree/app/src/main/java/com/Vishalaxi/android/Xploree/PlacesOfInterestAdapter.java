
package com.Vishalaxi.android.Xploree;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

public class PlacesOfInterestAdapter extends BaseAdapter {

  private final Context mContext;
  private final PlacesOfInterest[] mCategoryOfPOIs;

  public PlacesOfInterestAdapter(Context context, PlacesOfInterest[] categoryOfPOIs) {
    this.mContext = context;
    this.mCategoryOfPOIs = categoryOfPOIs;
  }

  @Override
  public int getCount() {
    return mCategoryOfPOIs.length;
  }

  @Override
  public long getItemId(int position) {
    return 0;
  }

  @Override
  public Object getItem(int position) {
    return null;
  }

  @Override
  public View getView(int position, View convertView, ViewGroup parent) {
    final PlacesOfInterest categoryPOI = mCategoryOfPOIs[position];
    // view holder pattern
    if (convertView == null) {
      final LayoutInflater layoutInflater = LayoutInflater.from(mContext);
      convertView = layoutInflater.inflate(R.layout.place_category_iv, null);

      final ImageView imageViewCoverArt = (ImageView)convertView.findViewById(R.id.imageview_cover_art);
      final ViewHolder viewHolder = new ViewHolder(imageViewCoverArt);
      convertView.setTag(viewHolder);
    }
    final ViewHolder viewHolder = (ViewHolder)convertView.getTag();
    viewHolder.imageViewCoverArt.setScaleType(ImageView.ScaleType.FIT_CENTER);
    viewHolder.imageViewCoverArt.setImageResource( categoryPOI.getImageResource());
    return convertView;
  }

  private class ViewHolder {
    private final ImageView imageViewCoverArt;
    public ViewHolder(ImageView imageViewCoverArt){
      this.imageViewCoverArt = imageViewCoverArt;
    }
  }

}
