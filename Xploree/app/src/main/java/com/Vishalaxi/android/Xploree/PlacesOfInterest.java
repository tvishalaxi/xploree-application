
package com.Vishalaxi.android.Xploree;

public class PlacesOfInterest {
    private final int imageResource;
    public int id;

    public PlacesOfInterest(int id, int imageResource) {
        this.imageResource = imageResource;
        this.id = id;
    }

    public int getImageResource() {
        return imageResource;
    }

}
