package com.Vishalaxi.android.Xploree;

import android.graphics.Bitmap;
import android.support.v4.app.FragmentManager;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by VISH on 8/15/2016.
 */
public class MapFragment extends Fragment {
    private View rootView;
    private MapView mMapView;
    private GoogleMap googleMap;
    private Map<String, Place> mHMReference_1;  // Links marker id and place object
    private Map<String, Bitmap> mHMReference_2; // Links marker id and bitmap object
    private Place[] localPlacesList;
    private double mLatitude, mLongitude;
    private static final float UNDEFINED_COLOR = -1;    // Specifies the drawMarker() to draw the marker with default color

    MapFragment(Place[] localPlacesList, double latitude, double longitude) {
        this.localPlacesList = localPlacesList;
        this.mLatitude = latitude;
        this.mLongitude = longitude;
        Log.i("testing", "Map fragment const latitude=" + latitude);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // inflate and return the layout
        rootView = inflater.inflate(R.layout.fragment_map, container, false);
        mMapView = (MapView) rootView.findViewById(R.id.mapView);
        mHMReference_1 = new HashMap<String, Place>();
        mHMReference_2 = new HashMap<String, Bitmap>();
        mMapView.onCreate(savedInstanceState);
        mMapView.onResume();// needed to get the map to display immediately
        try {
            MapsInitializer.initialize(getActivity().getApplicationContext());
        } catch (Exception e) {
            e.printStackTrace();
        }
        int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(getContext());
        if (status != ConnectionResult.SUCCESS) { // Google Play Services are not available
            int requestCode = 10;
            Dialog dialog = GooglePlayServicesUtil.getErrorDialog(status, getActivity(), requestCode);
            dialog.show();
        } else { // Google Play Services are available
            setupGoogleMaps();
        }
        return rootView;
    }

    public void setupGoogleMaps() {
        mMapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap mMap) {
                googleMap = mMap;
                try {
                    googleMap.setMyLocationEnabled(true);
                } catch (SecurityException se) {
                    se.printStackTrace();
                }
                //draw marker for your current location
                LatLng myLatLng = new LatLng(mLatitude, mLongitude);
                drawMarker(myLatLng, 125);

                //draw the marker for all the nearby places
                double lat = 0, lng = 0;
                for (int i = 0; i < localPlacesList.length; i++) {
                    lat = convertStringTODouble(localPlacesList[i].getLatitude());
                    lng = convertStringTODouble(localPlacesList[i].getLongitude());
                    LatLng placeLatLng = new LatLng(lat, lng);
                    Marker m = drawMarker(placeLatLng, 20);
                    mHMReference_1.put(m.getId(), localPlacesList[i]);
                    mHMReference_2.put(m.getId(), MainActivity.bitmapArray[i]);
                }
                googleMap.setOnMarkerClickListener(mapMarkerListener);
                configureCameraAtCurrentLocation();
            }
        });
    }

    public void configureCameraAtCurrentLocation() {
        LatLng currentLatLng = new LatLng(mLatitude, mLongitude);
        CameraPosition cameraPosition = new CameraPosition.Builder().target(currentLatLng).zoom(13).build();
        googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
    }

    private double convertStringTODouble(String strLat) {
        double dVal = 0;
        if (strLat != "") {
            dVal = (Double.parseDouble(strLat));
        }
        return dVal;
    }

    /* Map Marker click listener*/
    private GoogleMap.OnMarkerClickListener mapMarkerListener = new GoogleMap.OnMarkerClickListener() {
        @Override
        public boolean onMarkerClick(Marker marker) {
            // If not touched on markers
            if (!mHMReference_1.containsKey(marker.getId()))
                return false;
            if (!mHMReference_2.containsKey(marker.getId()))
                return false;
            // Getting place object corresponding to the currently clicked Marker
            Place place = mHMReference_1.get(marker.getId());
            Bitmap place_bitmap = mHMReference_2.get(marker.getId());
            // Creating a dialog fragment to display the photo
            PlaceDialogFragment dialogFragment = new PlaceDialogFragment(place, place_bitmap);
            // Getting a reference to Fragment Manager
            FragmentManager fm = getActivity().getSupportFragmentManager();
            // Starting Fragment Transaction
            FragmentTransaction ft = fm.beginTransaction();
            // Adding the dialog fragment to the transaction
            ft.add(dialogFragment, "TAG");
            // Committing the fragment transaction
            ft.commit();
            return false;
        }
    };

    /* Drawing marker at latLng with color*/
    private Marker drawMarker(LatLng latLng, float color) {
        // Creating a marker
        MarkerOptions markerOptions = new MarkerOptions();
        // Setting the position for the marker
        markerOptions.position(latLng);
        if (color != UNDEFINED_COLOR)
            markerOptions.icon(BitmapDescriptorFactory.defaultMarker(color));
        // Placing a marker on the touched position
        Marker m = googleMap.addMarker(markerOptions);
        return m;
    }

    @Override
    public void onResume() {
        super.onResume();
        mMapView.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mMapView.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mMapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mMapView.onLowMemory();
    }
}