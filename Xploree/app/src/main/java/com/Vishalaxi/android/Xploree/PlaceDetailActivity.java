package com.Vishalaxi.android.Xploree;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

/*Provides UI for the Detail page.*/
public class PlaceDetailActivity extends AppCompatActivity {

    public static final String EXTRA_POSITION = "position";
    private TextView placeDetail, placeLocation, placeRating, placeContact, placeWebsite, placeOpenNow;
    private ImageView placePicutre;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        setSupportActionBar((Toolbar) findViewById(R.id.toolbar));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        // Set Collapsing Toolbar layout to the screen
        CollapsingToolbarLayout collapsingToolbar = (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar);
        int postion = getIntent().getIntExtra(EXTRA_POSITION, 0);
        FragmentsActivity.fragment_type=getIntent().getIntExtra(TileContentFragment.EXTRA_FRAGMENT,1);
        Place myParceledPlace = (Place) getIntent().getParcelableExtra(TileContentFragment.EXTRA_PARCELABLE_PLACE);
        collapsingToolbar.setTitle(myParceledPlace.getPlaceName());

        String na="N/A";
        placeDetail = (TextView) findViewById(R.id.place_detail);
        String pd= myParceledPlace.getReviews();
        if(pd!=null) {
            placeDetail.setText(pd);
        }else{
            placeDetail.setText(na);
        }
        placeLocation =  (TextView) findViewById(R.id.place_location);
        String pv= myParceledPlace.getVicinity();
        if(pv!=null) {
            placeLocation.setText(pv);
        }else{
            placeLocation.setText(na);
        }

        placeRating =  (TextView) findViewById(R.id.place_rating);
        String pr= myParceledPlace.getRating();
        if(pr!=null) {
            placeRating.setText(pr);
        }else{
            placeRating.setText(na);
        }

        placeContact =  (TextView) findViewById(R.id.place_phoneNo);
        String pc= myParceledPlace.getPhoneNo();
        if(pc!=null) {
            placeContact.setText(pc);
        }else{
            placeContact.setText(na);
        }

        placeWebsite =  (TextView) findViewById(R.id.place_website);
        String pw= myParceledPlace.getWebsite();
        if(pw!=null) {
            placeWebsite.setText(pw);
        }else{
            placeWebsite.setText(na);
        }

        placeOpenNow =  (TextView) findViewById(R.id.place_openNow);
        String pon= myParceledPlace.getOpenNow();
        if(pon!=null) {
            placeOpenNow.setText(pon);
        }else{
            placeWebsite.setText(na);
        }

        placePicutre = (ImageView) findViewById(R.id.image);
        if(MainActivity.bitmapArray[postion]!=null) {
            placePicutre.setImageBitmap(MainActivity.bitmapArray[postion]);
        }else{
            placePicutre.setImageResource(R.drawable.no_photo);
            placePicutre.setAlpha(0.3f);
        }
    }

    @Override
    public void onBackPressed(){
       Log.i("testing","onBackPressed");
        //getSupportFragmentManager().popBackStackImmediate();
        FragmentManager fm = getSupportFragmentManager();
        if (fm.getBackStackEntryCount() > 0) {
            getSupportFragmentManager().popBackStackImmediate();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
