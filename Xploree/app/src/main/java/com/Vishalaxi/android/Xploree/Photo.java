package com.Vishalaxi.android.Xploree;

import android.os.Parcel;
import android.os.Parcelable;

public class Photo implements Parcelable {
    private int mWidth = 0; // Width of the Photo
    private int mHeight = 0; // Height of the Photo
    private String mPhotoReference = "";// Reference of the photo to be used in Google Web Services

    int getWidth() {
        return mWidth;
    }
    int getHeight() {
        return mHeight;
    }
    String getPhotoReference() {
        return mPhotoReference;
    }
    void setWidth(int width) {
        mWidth = width;
    }
    void setHeight(int height) {
        mHeight = height;
    }
    void setPhotoReference(String photoReference) {
        mPhotoReference = photoReference;
    }
    @Override
    public int describeContents() {
        // TODO Auto-generated method stub
        return 0;
    }
    /* Writing Photo object data to Parcel*/
    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(mWidth);
        dest.writeInt(mHeight);
        dest.writeString(mPhotoReference);
    }

    public Photo() {}

    /* Initializing Photo object from Parcel object*/
    private Photo(Parcel in) {
        this.mWidth = in.readInt();
        this.mHeight = in.readInt();
        this.mPhotoReference = in.readString();
    }

    /* Generates an instance of Place class from Parcel*/
    public static final Parcelable.Creator<Photo> CREATOR = new Parcelable.Creator<Photo>() {
        @Override
        public Photo createFromParcel(Parcel source) {
            return new Photo(source);
        }

        @Override
        public Photo[] newArray(int size) {
            // TODO Auto-generated method stub
            return null;
        }
    };
}