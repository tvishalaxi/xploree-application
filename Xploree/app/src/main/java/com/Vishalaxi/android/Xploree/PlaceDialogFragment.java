package com.Vishalaxi.android.Xploree;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewFlipper;

/*Defining DialogFragment class to show the place details with photo in the map screen*/
public class PlaceDialogFragment extends DialogFragment{

    private TextView mTVPhotosCount = null;
    private TextView mTVVicinity = null;
    private ViewFlipper mFlipper = null;
    ImageView iView = null;
    private Place mPlace = null;
    private Bitmap mBitmap;

    public PlaceDialogFragment(){
        super();
    }

    public PlaceDialogFragment(Place place, Bitmap bitmap){
        super();
        this.mPlace = place;
        this.mBitmap = bitmap;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        // For retaining the fragment on screen rotation
        setRetainInstance(true);
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.dialog_layout, null);

        mFlipper = (ViewFlipper) v.findViewById(R.id.flipper); // Getting reference to ViewFlipper
        mTVPhotosCount = (TextView) v.findViewById(R.id.tv_photos_count);// Getting reference to display photo count
        mTVVicinity = (TextView) v.findViewById(R.id.tv_vicinity); // Getting reference to display place vicinity
        if(mPlace!=null){
            getDialog().setTitle(mPlace.getPlaceName());// Setting the title for the Dialog Fragment
            Photo[] photos = mPlace.getPhotos();// Array of references of the photos
            mTVPhotosCount.setText("Photos available : " + photos.length); // Setting Photos count
            mTVVicinity.setText(mPlace.getVicinity());// Setting the vicinity of the place
        }
        iView = new ImageView(getActivity().getBaseContext());
        // Setting the downloaded image in ImageView
        if(mBitmap == null){
            iView.setImageResource(R.drawable.no_image);
        }else {
            iView.setImageBitmap(mBitmap);
        }
        // Adding the ImageView to ViewFlipper
        mFlipper.addView(iView);
        Toast.makeText(getActivity().getBaseContext(), "Image loaded successfully", Toast.LENGTH_SHORT).show();
        return v;
    }

    @Override
    public void onDestroyView() {
        if (getDialog() != null && getRetainInstance())
            getDialog().setDismissMessage(null);
        super.onDestroyView();
    }
}