package com.Vishalaxi.android.Xploree;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import android.util.Log;

public class PlaceJSONParser {

    /** Receives a JSONObject and returns a list of places */
    public Place[] parse(JSONObject jObject){
        JSONArray jPlaces = null;
        try {
            /** Retrieves all the elements in the 'places' array */
            jPlaces = jObject.getJSONArray("results");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return getPlaces(jPlaces);
    }
    /** Receives a JSONArray and returns a list of places */
    private Place[] getPlaces(JSONArray jPlaces){
        int placesCount = jPlaces.length();
        Place[] places = new Place[placesCount];
        /** Taking each place, parses and adds to list object */
        for(int i=0; i<placesCount;i++){
            try {
                /*Call getPlace with place JSON object to parse the place */
                places[i] = getPlace((JSONObject)jPlaces.get(i));

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return places;
    }

    /** Parsing the Place JSON object */
    private Place getPlace(JSONObject jPlace){
        Place place = new Place();
        try {
            // Extracting Place details, if available
            if(!jPlace.isNull("name")){
                place.setPlaceName(jPlace.getString("name"));
            }
            if(!jPlace.isNull("vicinity")){
                place.setVicinity(jPlace.getString("vicinity"));
            }
            if(!jPlace.isNull("photos")){
                JSONArray photos = jPlace.getJSONArray("photos");
                place.setPhotos(new Photo[photos.length()]);
                for(int i=0;i<photos.length();i++){
                    place.getPhotos()[i] = new Photo();
                    place.getPhotos()[i].setWidth(((JSONObject)photos.get(i)).getInt("width"));
                    place.getPhotos()[i].setHeight(((JSONObject)photos.get(i)).getInt("height"));
                    place.getPhotos()[i].setPhotoReference(((JSONObject)photos.get(i)).getString("photo_reference"));
                }
            }
            if(!jPlace.isNull("geometry") ){
                place.setLatitude(jPlace.getJSONObject("geometry").getJSONObject("location").getString("lat"));
                place.setLongitude(jPlace.getJSONObject("geometry").getJSONObject("location").getString("lng"));
            }
            if(!jPlace.isNull("place_id")){
                place.setPlaceID(jPlace.getString("place_id"));
            }
            if(!jPlace.isNull("rating")){
                place.setRating(jPlace.getString("rating"));
            }
            if(!jPlace.isNull("international_phone_number")){
                place.setPhoneNo(jPlace.getString("international_phone_number"));
            }
            if(!jPlace.isNull("opening_hours")){
                String openNow="Not Known";
                openNow= jPlace.getJSONObject("opening_hours").getString("open_now");
                if(openNow.charAt(0)=='t'){
                    place.setOpenNow("YES");
                }else{
                    place.setOpenNow("NO");
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
            Log.d("EXCEPTION", e.toString());
        }
        return place;
    }
}